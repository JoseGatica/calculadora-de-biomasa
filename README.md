# Calculadora de Biomasa
#### Proyecto basado en Python, desarrollado para cliente externo.
<br>
"Calculadora de Biomasa" es una solución desarrollada para el cliente <b>Efecto Visual</b>.
<br>
<br>

## Librerías exportadas:

- pd
- pandas
- openpyxl

Este desarrollo busca facilitar la obtención de datos desde un ambiente web, llevarlo a libro excel alojado en el servidor y devolver al ambiente web el cálculo de los resultados según los datos ingresados por el usuario.

Los datos devueltos son los datos arrojados por otra hoja ("Resultados") del mismo libro excel.

Esto evitaría estar compartiendo el archivo y en su lugar los cálculos pueden ser llevados a cabo con sólo ingresar al sitio web.
